'use strict'
require('dotenv').config()

const mongoose = require('mongoose')
const sheetOps = require('./sheet-ops')


const PathSchema = mongoose.Schema({
	name : {
		type : String,
	},
	isDefaultTeacher : Boolean,
	isDefaultStudent : Boolean,
	flows : [{
		type : mongoose.Schema.Types.ObjectId
	}]
}) 

const FlowSchema = mongoose.Schema({
	name : String
})

const Path = mongoose.model('Path', PathSchema)
const Flow = mongoose.model('Flow', FlowSchema)


let main = async () => {	
	try{
		console.warn(process.env.SUPER_SECRET)
		let content = sheetOps.getJsonContent('sample.xlsx')
		await mongoose.connect('mongodb://localhost/js-course', {useNewUrlParser: true})
		for (let currentPath of content){
			let path = new Path()
			path.name = currentPath.name
			path.isDefaultStudent = currentPath.isDefaultStudent
			path.isDefaultTeacher = currentPath.isDefaultTeacher
			for (let currentFlow of currentPath.flows){
				let flow = new Flow()
				flow.name = currentFlow.name
				path.flows.push(flow)
				await flow.save()
			}
			await path.save()
		}
	}
	catch (e){
		console.warn(e)
	}
}

main()
