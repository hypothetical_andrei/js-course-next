'use strict'
const xlsx = require('node-xlsx')
const fs = require('fs')

// console.warn(xlsx)
const SAMPLE_PATH = './test-file'
const SAMPLE_XLSX = 'sample.xlsx'
const OUT_FILE = 'sample.json'
// fs.readFile(SAMPLE_PATH, (err, data)  => {
// 	if (!err){
// 		console.warn(data.toString('utf-8'))
// 	}
// })

// let content = fs.readFileSync(SAMPLE_PATH)
// console.warn(content.toString('utf-8'))

const getJsonContent = (fileName) => {
	let sheets = xlsx.parse(`${__dirname}/${fileName}`)

	let activeSheet = sheets[0]

	let sheetData = activeSheet.data

	let firstProcessed = false
	let colHeadings
	
	let paths = []

	for (let row of sheetData){
		if (!firstProcessed){
			colHeadings = row
			firstProcessed = true
			continue
		}
		if (row[0]){
			let currentPath = {}
			currentPath.name = row[0]
			currentPath.isDefaultTeacher = row[1]
			currentPath.isDefaultStudent = row[2]
			currentPath.flows = []
			paths.push(currentPath)	
		}
		else{
			continue
		}
	}

	return paths
}

module.exports = {
	getJsonContent
}